import sys, os
import re
import operator
if len(sys.argv) != 2:
    sys.exit("This is a file parser for cardinal player batting average. You need to input one argument which is the file path to the text file.")

filename = sys.argv[1]

if not os.path.exists(filename):
    sys.exit("Error: File is not found!")

f = open(filename)


b = {}
batting_re = re.compile(r"([\w]+) ([\w]+) batted ([\d]+) times with ([\d]+) hits and [\d]+ runs")
for line in f:
    match = batting_re.match(line.rstrip())
    if match is not None:
        name = match.group(1) + " " + match.group(2)
        if b.has_key(name):
            temp = b[name]
            b[name] = (temp[0]+ int(match.group(3)), temp[1]+ int(match.group(4)))
        else:
            b[name] = (int(match.group(3)), int(match.group(4)))

averages = {}
for k in b:
    elem = b[k]
    avg = float(elem[1])/float(elem[0])
    averages[k]=avg
    
for w in sorted(averages, key=averages.get, reverse=True):
    truncate = str(round(averages[w],3))
    if len(truncate)<5:
        truncate = truncate + "0"
    print w + ": " + truncate
    
f.close()